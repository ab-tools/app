import {authHelper} from "../common/auth";
import {Provider} from "use-http";
import {notify} from "../common/notify";
import {AppRouter} from "../common/routers";

export default function App () {
    let pathname = "/app"

    let httpOptions = {
        cachePolicy: "no-cache",
        interceptors: {
            // every time we make an http request, this will run 1st before the request is made
            request: async ({options}) => {
                let accessToken = await authHelper.refreshAccessToken()
                if (accessToken) {
                    options.headers.Authorization = `Bearer ${accessToken}`
                    return options
                }
            },
            response: async ({response}) => {
                const res = response
                if (res.status === 404) {
                    notify("warning", "Not Found", "Could not find the requested resource")
                }
                if (res.status === 400) {
                    if(res.data !== undefined && res.data.message !== undefined){
                        notify("warning", "Error", res.data.message)
                    }
                }
                return res
            }
        }
    }

    return (
        <Provider url={process.env.REACT_APP_API_URL} options={httpOptions}>
                <AppRouter pathname={pathname}/>
        </Provider>
    )
}
