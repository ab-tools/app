export let formatDate = timestamp => {
    let ts = new Date(Date.parse(timestamp))
    let options = {year: 'numeric', month: 'long', day: 'numeric' }

    return ts.toLocaleDateString("en-US", options)
}
