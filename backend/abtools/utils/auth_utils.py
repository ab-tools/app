from rest_framework.exceptions import AuthenticationFailed

from abtools.experiments.models import Experiment, Variation
from abtools.workspaces.models import Workspace, Member


def check_auth(instance, user):
    user_workspaces = Workspace.objects.filter(member__user=user)
    message = "Cannot delete {} - Unauthorized"
    if isinstance(instance, Workspace):
        if instance in user_workspaces:
            return True
        raise AuthenticationFailed(message.format("Workspace"))
    elif isinstance(instance, Experiment):
        if instance.workspace in user_workspaces:
            return True
        raise AuthenticationFailed(message.format("Experiment"))
    elif isinstance(instance, Variation):
        if instance.experiment.workspace in user_workspaces:
            return True
        raise AuthenticationFailed(message.format("Variation"))
    elif isinstance(instance, Member):
        if instance.workspace in user_workspaces:
            return True
        raise AuthenticationFailed(message.format("Member"))

    raise AuthenticationFailed("Unauthorized")
