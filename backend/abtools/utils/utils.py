import uuid


def make_slug():
    return str(uuid.uuid4())[-5:].upper()
