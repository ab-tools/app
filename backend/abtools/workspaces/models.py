import datetime

from django.conf import settings
from django.contrib.postgres.constraints import ExclusionConstraint
from django.contrib.postgres.fields import DateRangeField, RangeOperators
from django.db import models
from model_utils.models import TimeStampedModel, UUIDModel
from rest_framework.exceptions import ValidationError

from abtools.utils.utils import make_slug


class Workspace(TimeStampedModel, UUIDModel):
    name = models.CharField(null=False, max_length=100, default="New Workspace")
    slug = models.SlugField(null=False, max_length=20, unique=True, default=make_slug)
    description = models.CharField(null=True, max_length=200, blank=True)

    def __str__(self):
        return f"ws {self.name} sub"


class Subscription(TimeStampedModel, UUIDModel):
    workspace = models.OneToOneField(Workspace, null=True, on_delete=models.CASCADE, related_name="subscription")

    def __str__(self):
        return self.workspace.name


class Quota(TimeStampedModel, UUIDModel):
    subscription = models.ForeignKey(Subscription, null=False, on_delete=models.CASCADE)
    period = DateRangeField(null=False)
    is_trial = models.BooleanField(null=False, default=False)
    allowance = models.IntegerField(null=False, default=10 * 1000)
    usage = models.IntegerField(null=False, default=0)

    class Meta:
        # Requires BtreeGistExtension https://docs.djangoproject.com/en/4.0/ref/contrib/postgres/operations/
        constraints = [
            ExclusionConstraint(
                name='exclude_overlapping_quotas',
                expressions=[
                    ('period', RangeOperators.OVERLAPS),
                    ('subscription', RangeOperators.EQUAL),
                ],
            )]

    def quota_usage(self, remaining=True):
        if remaining:
            return self.allowance - self.usage, 1 - self.usage / self.allowance
        else:
            return self.usage, self.usage / self.allowance


    def is_current(self):
        return self.period.lower <= datetime.date.today() < self.period.upper


class Member(TimeStampedModel, UUIDModel):
    workspace = models.ForeignKey(Workspace, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             null=False,
                             on_delete=models.CASCADE)

    class Meta:
        unique_together = [["user", "workspace"]]

    def delete(self, *args, **kwargs):
        workspace_members = Member.objects.filter(workspace=self.workspace)
        if len(workspace_members) == 1:
            raise ValidationError("Cannot remove last member of workspace")
        super(Member, self).delete(*args, **kwargs)
