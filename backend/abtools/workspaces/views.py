from django_filters import rest_framework as filters
from rest_framework import viewsets

from .models import Workspace, Member, Quota
from .serializers import WorkspaceSerializer, QuotaSerializer


class QuotaFilter(filters.FilterSet):
    subscription__workspace__slug = filters.CharFilter()

    class Meta:
        fields = ['subscription__workspace__slug', ]
        model = Quota


class QuotaViewSet(viewsets.ModelViewSet):
    serializer_class = QuotaSerializer
    queryset = Quota.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = QuotaFilter
    http_method_names = ['get']

    def get_queryset(self):
        user = self.request.user
        return Quota.objects.filter(subscription__workspace__member__user=user).order_by("period")


class WorkspaceViewSet(viewsets.ModelViewSet):
    serializer_class = WorkspaceSerializer
    queryset = Workspace.objects.all()
    filterset_fields = ['slug']
    http_method_names = ['get', 'post', "patch", 'head', "delete"]

    def get_queryset(self):
        user = self.request.user
        return Workspace.objects.filter(member__user=user)

    def perform_create(self, serializer):
        instance = serializer.save()
        Member.objects.get_or_create(
            workspace=instance,
            user=self.request.user)
