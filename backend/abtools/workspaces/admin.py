from django.contrib import admin
from .models import Workspace, Member, Quota, Subscription


class MemberInline(admin.StackedInline):
    model = Member


class WorkSpaceAdmin(admin.ModelAdmin):
    model = Workspace
    inlines = [MemberInline]
    list_display = ('name', 'id', "slug")
    readonly_fields = ["id"]


class QuotaInline(admin.StackedInline):
    model = Quota

class SubscriptionAdmin(admin.ModelAdmin):
    model = Workspace
    inlines = [QuotaInline]

admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(Workspace, WorkSpaceAdmin)
