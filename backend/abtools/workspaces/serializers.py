import datetime

from rest_framework import serializers

from .models import Workspace, Quota


class WorkspaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Workspace
        fields = "__all__"


class QuotaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quota
        fields = ('period', "allowance", "usage", "is_current")


