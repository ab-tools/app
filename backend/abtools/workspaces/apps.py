from django.apps import AppConfig


class WorkspacesConfig(AppConfig):
    name = 'abtools.workspaces'

    def ready(self):
        import abtools.workspaces.signals
