import datetime

from django.db.models.signals import post_save

from .models import Workspace, Subscription, Quota


def create_workspace(sender, instance, created, **kwargs):
    if created:
        subscription = Subscription.objects.create(workspace=instance)
        quota = Quota.objects.create(is_trial=True,
                                     subscription=subscription,
                                     period=(
                                             datetime.date.today(),
                                             datetime.date.today() + datetime.timedelta(days=30),
                                         ))


post_save.connect(create_workspace, Workspace)
