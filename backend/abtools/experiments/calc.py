import statsmodels.stats.api as sms
from dataclasses import dataclass

import arviz as az  # Only required for calculating HDI
import numpy as np
from scipy.stats import beta

az.rcParams['stats.hdi_prob'] = 0.95  # HDI interval at .95


@dataclass
class ModelConfig:
    prior_alpha: int
    prior_beta: int
    rope_pct: int

    power: float
    significance: float


@dataclass
class SampleSize:
    converged: bool
    required: int
    remaining: int


@dataclass
class ModelInput:
    """
    trials_a: all participants, control-group
    trials_b: all participants, test-group
    hits_a: successful conversions, control-group
    hits_b: successful conversions, test-group
    """
    trials_a: int
    trials_b: int
    hits_a: int
    hits_b: int


class Calculator:
    def __init__(self, model_input: ModelInput, model_config: ModelConfig):
        self.n_trials = 100 * 1000
        self.non_hits_a = model_input.trials_a - model_input.hits_a
        self.non_hits_b = model_input.trials_b - model_input.hits_b
        self.hits_a = model_input.hits_a
        self.hits_b = model_input.hits_b
        self.prior_alpha = model_config.prior_alpha
        self.prior_beta = model_config.prior_beta
        self.rope_pct = model_config.rope_pct / 100  # Convert ROPE to float representation

        self.power = model_config.power
        self.significance = model_config.significance

        # Calculated values
        self.a_samples = None
        self.b_samples = None
        self.posterior_dist = None
        self.prob_b_eob = None
        self.uplift_b_mean = None
        self.ecdf_x = None
        self.ecdf_y = None
        self.hdi_lo = None
        self.hdi_hi = None
        self.a_mean = None
        self.b_mean = None
        self.a_var = None
        self.b_var = None
        self.a_skew = None
        self.b_skew = None
        self.a_kurt = None
        self.b_kurt = None
        self.sample_size = SampleSize(False, 100**3-1, 100**3-1)

    def compute_sample_size(self) -> SampleSize:
        """
        Parameters
        ----------
        p1 : float
            The baseline proportion, e.g. conversion rate

        min_diff : float
            Minimum detectable difference

        significance : float, default 0.05
            Frequently also denoted as alpha. Governs the chance of a false positive.
            Example: At significance = 0.05, there is a 5% chance of
            obtaining a false positive. Thus, our confidence level is
            1 - 0.05 = 0.95

        power : float, default 0.8
            Frequently also denoted as beta. Power of 0.80 means that there is an 80%
            chance that if there was an effect, we would detect it. Conversly
            there is a 20% chance of not detecting a true effect

        Returns
        -------
        sample_size: DataClass

        """
        sample_size = SampleSize(False, 100 ** 3 - 1, 100 ** 3 - 1)

        cvo_a = self.hits_a / (self.non_hits_a + self.hits_a)
        cvo_b = self.hits_b / (self.non_hits_b + self.hits_b)

        if cvo_a == cvo_b:
            return sample_size
        p1 = cvo_a
        min_diff = abs(cvo_b - cvo_a)

        p2 = p1 + min_diff
        effect_size = sms.proportion_effectsize(p1, p2)
        required_sample_size = sms.NormalIndPower().solve_power(
            effect_size, power=self.power, alpha=self.significance, ratio=1)

        required_sample_size = required_sample_size * 2

        remaining_required_sample_size = required_sample_size - sum(
            [self.hits_a, self.non_hits_a, self.hits_b, self.non_hits_b])

        return SampleSize(True, required_sample_size, remaining_required_sample_size)

    def compute_posterior(self):
        self.a_samples = beta.rvs(self.prior_alpha + self.hits_a, self.prior_beta + self.non_hits_a, size=self.n_trials)
        self.b_samples = beta.rvs(self.prior_alpha + self.hits_b, self.prior_beta + self.non_hits_b, size=self.n_trials)
        self.posterior_dist = self.b_samples / self.a_samples - 1

    def compute_stats(self):
        self.prob_b_eob = round(sum(self.posterior_dist >= 0) / self.n_trials, 4)
        hdi = az.hdi(self.posterior_dist, hdi_prob=.94)
        self.uplift_b_mean = np.mean(self.posterior_dist)
        self.hdi_lo = round(hdi[0], 4)
        self.hdi_hi = round(hdi[1], 4)
        self.sample_size = self.compute_sample_size()

    def compute_ecdf(self):
        self.ecdf_x = np.sort(self.posterior_dist)
        self.ecdf_y = np.arange(len(self.b_samples)) / float(len(self.b_samples))

    def compute_moments(self):
        self.a_mean, self.a_var, self.a_skew, self.a_kurt = beta.stats(self.hits_a + self.prior_alpha,
                                                                       self.non_hits_a + self.prior_beta,
                                                                       moments='mvsk')
        self.b_mean, self.b_var, self.b_skew, self.b_kurt = beta.stats(self.hits_b + self.prior_alpha,
                                                                       self.non_hits_a + self.prior_beta,
                                                                       moments='mvsk')

    def compute_stop_recommendation(self):
        """
        Provides stopping criterion by comparing 5th and 95th percentile of posterior distribution with ROPE
        :return:
        """

        rope_max = self.rope_pct
        rope_min = -1 * self.rope_pct

        if self.hdi_lo >= rope_min and self.hdi_hi <= rope_max:
            return {
                "stop": True,
                "winner": None,
            }
        elif self.hdi_lo <= rope_max and rope_min <= self.hdi_hi:
            return {
                "stop": False,
                "winner": False,
            }
        else:
            return {
                "stop": True,
                "winner": "test" if self.b_mean >= self.a_mean else "control"
            }

    def run(self):
        self.compute_sample_size()
        self.compute_posterior()
        self.compute_stats()
        self.compute_ecdf()
        self.compute_moments()
        recommendation = self.compute_stop_recommendation()

        stats = dict(
            hdi_lo=self.hdi_lo,
            hdi_hi=self.hdi_hi,
            prob_test_eob=self.prob_b_eob,
            a_mean=self.a_mean,
            b_mean=self.b_mean,
            a_sd=self.a_var ** (1 / 2),
            b_sd=self.b_var ** (1 / 2),
        )
        result = dict(
            plot_ecdf=dict(
                x=self.ecdf_x[::250],
                y=self.ecdf_y[::250],
            ),
            recommendation=recommendation,
            stats=stats,
            sample_size=self.sample_size.__dict__,
        )
        return result
