from django.apps import AppConfig


class ExperimentsConfig(AppConfig):
    name = 'abtools.experiments'

    def ready(self):
        import abtools.experiments.signals
        # Makes sure all signal handlers are connected
        import abtools.experiments.handlers  # noqa
