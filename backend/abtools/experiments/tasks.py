from config import celery_app
import datetime

from abtools.workspaces.models import Quota
from abtools.experiments.models import Variation, StatEvent
from .models import Workspace


@celery_app.task()
def track_stat_event(ws_slug,
                     var_slug,
                     is_mobile,
                     is_tablet,
                     is_touch_capable,
                     is_bot,
                     is_pc,
                     device_family,
                     browser_family,
                     os_family,
                     is_trial,
                     is_hit):
    if int(is_hit)+int(is_trial) != 1:
        return

    try:
        workspace = Workspace.objects.get(slug=ws_slug)
        variation = Variation.objects.get(slug=var_slug,
                                          experiment__workspace=workspace)
    except (Workspace.DoesNotExist, Variation.DoesNotExist) as e:
        return

    StatEvent.objects.create(
        is_mobile=is_mobile,
        is_tablet=is_tablet,
        is_touch_capable=is_touch_capable,
        is_pc=is_pc,
        is_bot=is_bot,

        device_family=device_family,
        browser_family=browser_family,
        os_family=os_family,

        variation=variation,
        hit=int(is_hit),
        trial=int(is_trial),
    )

    quota = Quota.objects.get(
        subscription__workspace=workspace,
        period__contains=datetime.date.today()
    )

    quota.usage += 1
    quota.save()
