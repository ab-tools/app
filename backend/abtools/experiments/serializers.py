from rest_framework import serializers

from .models import Experiment, Variation


class VariationSerializer(serializers.ModelSerializer):
    workspace_slug = serializers.SlugField(source='experiment.workspace.slug', read_only=True)
    experiment_slug = serializers.SlugField(source='experiment.slug', read_only=True)

    class Meta:
        model = Variation
        fields = "__all__"

class ExperimentSerializer(serializers.ModelSerializer):
    workspace_slug = serializers.SlugField(source='workspace.slug', read_only=True)

    class Meta:
        model = Experiment
        fields = "__all__"
