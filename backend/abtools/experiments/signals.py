from django.db.models.signals import post_save, pre_save
from rest_framework.exceptions import ValidationError
from .models import Experiment, Variation


def create_variation(sender, instance, **kwargs):
    workspace = instance.experiment.workspace
    variations = Variation.objects.filter(experiment__workspace=workspace, slug=instance.slug).exclude(id=instance.id)
    if len(variations) > 0:
        raise ValidationError("Variation slug must be unique per workspace")


def create_experiment(sender, instance, created, **kwargs):
    if created:
        for var_type in Variation.VariationType.choices:
            name = "Test" if var_type[0] == "Test" else "Control"
            Variation.objects.create(experiment=instance, type=var_type[0], name=name)


pre_save.connect(create_variation, Variation)
post_save.connect(create_experiment, Experiment)
