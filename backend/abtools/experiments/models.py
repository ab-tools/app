import datetime as dt

from django.db import models
from django.utils.translation import gettext_lazy as _
from model_utils.models import TimeStampedModel, UUIDModel

from abtools.utils.utils import make_slug
from abtools.workspaces.models import Workspace


def make_exepriment_description():
    "created on: " + dt.datetime.now().strftime("%Y-%m-%d %H:%s")


class Experiment(TimeStampedModel, UUIDModel):
    name = models.CharField(null=False, max_length=100, help_text="Experiment name ('Checkout redesign')",
                            default="New Experiment")
    description = models.CharField(null=True, max_length=200, default=make_exepriment_description)
    workspace = models.ForeignKey('workspaces.Workspace', to_field="slug", on_delete=models.CASCADE, null=False)
    slug = models.CharField(max_length=10, null=False, default=make_slug)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = [['slug', 'workspace'], ]


class Variation(TimeStampedModel, UUIDModel):
    class VariationType(models.TextChoices):
        CONTROL = 'Control', _('Control')
        TEST = 'Test', _('Test')

    experiment = models.ForeignKey(Experiment, null=False, on_delete=models.CASCADE)
    name = models.CharField(null=False, max_length=100,
                            help_text="Variation name ('neon submit button')")

    slug = models.CharField(null=False, max_length=20, default=make_slug)
    type = models.CharField(
        max_length=7,
        choices=VariationType.choices,
    )

    class Meta:
        unique_together = [["experiment", "type"], ["slug", "experiment"]]

    def __str__(self):
        return self.name


class StatEvent(TimeStampedModel, UUIDModel):
    is_mobile = models.BooleanField(null=False, default=False)
    is_tablet = models.BooleanField(null=False, default=False)
    is_touch_capable = models.BooleanField(null=False, default=False)
    is_pc = models.BooleanField(null=False, default=False)
    is_bot = models.BooleanField(null=False, default=False)

    device_family = models.CharField(max_length=30, null=False)
    browser_family = models.CharField(max_length=30, null=False)
    os_family = models.CharField(max_length=30, null=False)

    variation = models.ForeignKey(Variation, null=False, on_delete=models.CASCADE)

    trial = models.IntegerField(help_text="users, views ...", default=0)
    hit = models.IntegerField(help_text="hits, conversions, clicks...", default=0)
