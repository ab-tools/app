import json

from django.db.models import F
from django.db.models import Sum
from django.db.models.functions import TruncDate, Extract
from rest_framework import renderers
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError, AuthenticationFailed
from rest_framework.generics import get_object_or_404
from rest_framework.parsers import JSONParser, BaseParser
from rest_framework.response import Response
from rest_framework.views import APIView

from .calc import Calculator, ModelInput, ModelConfig
from .models import Variation, Experiment, StatEvent
from .serializers import ExperimentSerializer, VariationSerializer
from .tasks import track_stat_event
from ..utils.auth_utils import check_auth
from ..workspaces.models import Workspace


class ExperimentViewSet(viewsets.ModelViewSet):
    serializer_class = ExperimentSerializer
    queryset = Experiment.objects.all()
    filterset_fields = ['slug', "workspace"]
    http_method_names = ['get', 'post', "patch", 'head', "delete"]

    def get_queryset(self):
        user = self.request.user
        workspaces = Workspace.objects.filter(member__user=user)
        return Experiment.objects.filter(workspace__in=workspaces)

    @action(detail=True, renderer_classes=[renderers.JSONRenderer])
    def results(self, request, *args, **kwargs):
        experiment = self.get_object()
        experiment_results = get_experiment_results(experiment, detail=True)
        return Response({"data": experiment_results})

    @action(detail=True, renderer_classes=[renderers.JSONRenderer])
    def reset(self, request, *args, **kwargs):
        experiment = self.get_object()
        check_auth(experiment, request.user)

        StatEvent.objects.filter(variation__experiment=experiment).delete()

        return Response({"message": "ok"})


class VariationViewSet(viewsets.ModelViewSet):
    serializer_class = VariationSerializer
    queryset = Variation.objects.all()
    filterset_fields = ['experiment', ]
    http_method_names = ['get', 'post', "patch", 'head']

    def get_queryset(self):
        user = self.request.user
        workspaces = Workspace.objects.filter(member__user=user)
        return Variation.objects.filter(experiment__workspace__in=workspaces)


class PlainTextParser(BaseParser):
    """
    Plain text parser.
    """
    media_type = 'text/plain'

    def parse(self, stream, media_type=None, parser_context=None):
        """
        Simply return a string representing the body of the request.
        """
        input = stream.read()
        parsed = json.loads(input.decode("utf-8"))
        return parsed


class StatsTracker(APIView):
    authentication_classes = []
    parser_classes = [JSONParser, PlainTextParser]
    permission_classes = []

    def post(self, request):
        workspace_slug = request.data.get("workspace", "")
        variation_slug = request.data.get("variation", "")
        calltype = request.data.get("calltype", "")

        for x in [workspace_slug, variation_slug, calltype]:
            if not len(x) > 0:
                raise ValidationError({"message": "workspace, variation and calltype required"})

        if calltype not in ["hit", "trial"]:
            raise ValidationError({"message": "invalid calltype"})

        track_stat_event.delay(
            ws_slug=workspace_slug,
            var_slug=variation_slug,
            is_mobile=request.user_agent.is_mobile,
            is_tablet=request.user_agent.is_tablet,
            is_touch_capable=request.user_agent.is_touch_capable,
            is_pc=request.user_agent.is_pc,
            is_bot=request.user_agent.is_bot,
            device_family=request.user_agent.device.family,
            browser_family=request.user_agent.browser.family,
            os_family=request.user_agent.os.family,
            is_trial=calltype == "trial",
            is_hit=calltype == "hit"
        )

        return Response({"message": "ok"})


def validate_model_input(model_input: ModelInput):
    if any([x < 1 for x in [model_input.trials_a, model_input.trials_b,
                            model_input.hits_a, model_input.hits_b]]):
        raise ValidationError({"message": "Calculation Error: All trials/hits must be > 0"})

    if model_input.hits_a > model_input.trials_a or model_input.hits_b > model_input.trials_b:
        raise ValidationError({"message": "Calculation Error: Trial count must not exceed hit count"})


def validate_model_config(model_config: ModelConfig):
    try:
        model_config.rope_pct / 1.0
    except ValueError:
        raise ValidationError({"message": "ROPE must be numerical"})


def get_experiment_results(experiment, detail=False):
    experiment_results = (StatEvent.objects
                          .filter(variation__experiment=experiment)
                          .annotate(date=TruncDate('created'),
                                    hour=Extract('created', "hour"),
                                    variation_type=F('variation__type'))
                          )

    if detail:
        return list((experiment_results.values("date", "hour", "variation_type", "is_mobile")
                     .annotate(trials=Sum("trial"), hits=Sum("hit"))
                     .order_by("variation_type", "date", "hour")))
    else:
        return list((experiment_results.values("variation_type")
                     .annotate(trials=Sum("trial"), hits=Sum("hit"))
                     .order_by("variation_type")))


def get_model_input_from_experiment(experiment_id, user):
    experiment = get_object_or_404(Experiment, pk=experiment_id)
    user_workspaces = Workspace.objects.filter(member__user=user)
    if experiment.workspace not in user_workspaces:
        raise AuthenticationFailed({"message": "Unauthorized"})

    experiment_results = get_experiment_results(experiment)

    if len(experiment_results) != 2:
        raise ValidationError({"message": "Not enough data to perform calculation"})

    results_control = [d for d in experiment_results if d['variation_type'] == "Control"][0]
    results_test = [d for d in experiment_results if d['variation_type'] == "Test"][0]
    model_input = ModelInput(
        trials_a=int(results_control["trials"]),
        trials_b=int(results_test["trials"]),
        hits_a=int(results_control["hits"]),
        hits_b=int(results_test["hits"]),
    )

    return model_input


class CalcView(APIView):
    permission_classes = []

    @staticmethod
    def parse_request(request):
        data = request.data

        prior_alpha = data.get("prior_alpha", None)
        prior_beta = data.get("prior_beta", None)
        power = data.get("power", None)
        significance = data.get("significance", None)
        rope_pct = data.get("rope_pct", 1)

        model_config = ModelConfig(
            prior_alpha=prior_alpha if prior_alpha is not None else 1,
            prior_beta=prior_beta if prior_beta is not None else 1,
            rope_pct=abs(rope_pct),

            power=power if power is not None else 0.8,
            significance=significance if significance is not None else 0.05,

        )

        if any([prior_alpha, prior_beta]) and not all([prior_alpha, prior_beta]):
            raise ValidationError({"message": "Must provide two values for prior, or none"})

        experiment = data.get("experiment", None)

        if experiment is not None:
            if not request.user.is_authenticated:
                raise AuthenticationFailed({"message": "Must be logged in"})
            model_input = get_model_input_from_experiment(experiment, request.user)

        else:
            model_input = ModelInput(
                trials_a=int(data.get("trials_control")),
                trials_b=int(data.get("trials_test")),
                hits_a=int(data.get("hits_control")),
                hits_b=int(data.get("hits_test")),
            )

        return model_input, model_config

    def post(self, request, format=None):
        model_input, model_config = self.parse_request(request)

        validate_model_config(model_config)
        validate_model_input(model_input)

        results = Calculator(model_input, model_config).run()

        return Response({"message": "ok",
                         "data": results,
                         })
