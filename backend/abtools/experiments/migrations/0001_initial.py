# Generated by Django 3.2.10 on 2022-01-03 10:38

import abtools.experiments.models
import abtools.utils.utils
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Experiment',
            fields=[
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('id', model_utils.fields.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(default='New Experiment', help_text="Experiment name ('Checkout redesign')", max_length=100)),
                ('description', models.CharField(default=abtools.experiments.models.make_exepriment_description, max_length=200, null=True)),
                ('slug', models.CharField(default=abtools.utils.utils.make_slug, max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='RawEvent',
            fields=[
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('id', model_utils.fields.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('is_mobile', models.BooleanField(default=False)),
                ('is_tablet', models.BooleanField(default=False)),
                ('is_touch_capable', models.BooleanField(default=False)),
                ('is_pc', models.BooleanField(default=False)),
                ('is_bot', models.BooleanField(default=False)),
                ('device_family', models.CharField(max_length=30)),
                ('browser_family', models.CharField(max_length=30)),
                ('os_family', models.CharField(max_length=30)),
                ('variation_slug', models.CharField(max_length=20, null=True)),
                ('workspace_slug', models.CharField(max_length=20, null=True)),
                ('trial', models.IntegerField(default=0, help_text='users, views ...')),
                ('hit', models.IntegerField(default=0, help_text='hits, conversions, clicks...')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Variation',
            fields=[
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('id', model_utils.fields.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(help_text="Variation name ('neon submit button')", max_length=100)),
                ('slug', models.CharField(default=abtools.utils.utils.make_slug, max_length=20)),
                ('type', models.CharField(choices=[('Control', 'Control'), ('Test', 'Test')], max_length=7)),
                ('experiment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='experiments.experiment')),
            ],
        ),
    ]
