from django.contrib import admin

from .models import Experiment, Variation, StatEvent


class StatEventAdmin(admin.ModelAdmin):
    models = StatEvent


class VariationAdmin(admin.ModelAdmin):
    models = Variation

    list_display = ["experiment", "name", "slug"]


class ExperimentAdmin(admin.ModelAdmin):
    models = Experiment

    list_display = ["name"]


admin.site.register(StatEvent, StatEventAdmin)
admin.site.register(Experiment, ExperimentAdmin)
admin.site.register(Variation, VariationAdmin)
