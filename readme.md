# A-B Tools

<div align="center">
   <a href="https://a-b.tools">
      <img src="https://gitlab.com/ab-tools/app/-/raw/master/frontend/public/logo192.png" alt="A-B.Tools logo" align="center"/>
   </a>
</div>


[A-B.Tools](https://a-b.tools) is a lightweight and open-source A/B testing tool powered by Bayesian statistics.
Get easily interpretable results for your splits - no cookies or JavaScript required

<div align="center">
   <a href="https://a-b.tools">
      <img src="https://gitlab.com/ab-tools/app/-/raw/master/frontend/src/img/product3.webp" alt="A-B.Tools Product" align="center"/>
   </a>
</div>

## Why A-B.Tools
- **Get Results Fast** -  Our statistical models allow you to speed up your A/B-Tests by incorporating your domain knowlege 
- **No Stats Required** - A-B.Tools does all the data-crunching for you and delivers clear and actionable recommendations - no data-science skills required
- **Easy Integration** - Data collection via simple HTTP calls allows you to get started quickly and without taking on any extra dependencies. If your clients
run HTTP you can run A-B.Tools
- **Privacy Friendly and Open Source** - A-B.Tools does not store any personal user data or IP addresses. Don't trust us? See for 
yourself at  our [git-repository](https://gitlab.com/ab-tools/)