echo 'pulling changes from remote'
git pull

echo 'pull complete'
docker-compose build
echo 'build complete'

echo 'starting up app'
docker-compose up -d
echo 'start app complete'

echo 'performing migrations'
docker-compose run --rm abtools-django python manage.py migrate
echo 'migrations complete'

echo 'ALL DONE - EXITING'
exit
